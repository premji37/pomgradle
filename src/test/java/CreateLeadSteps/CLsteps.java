/*package CreateLeadSteps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CLsteps {
public FirefoxDriver driver;
@Given("Open The Browser")
public void openTheBrowser() {
   System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver_64bit.exe");
   driver=new FirefoxDriver();
}

@Given("Max the Browser")
public void maxTheBrowser() {
   driver.manage().window().maximize();
}

@Given("Set the TimeOut")
public void setTheTimeOut() {
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}

@Given("Launch The URL")
public void launchTheURL() {
    driver.get("http://leaftaps.com/opentaps/control/main");
}

@Given("Enter the UserName as (.*)")
public void enterTheUserNameAsDemoCRM(String username) {
  driver.findElementById("username").sendKeys(username);
}

@Given("Enter the Password as (.*)")
public void enterThePasswordAsCrmfsa(String password) {
	driver.findElementById("password").sendKeys(password);
}

@Given("click the Login button")
public void clickTheLoginButton() {
   driver.findElementByClassName("decorativeSubmit").click();
}
@Given("click the CRM Link")
public void clickTheCRMLink() {
   driver.findElementByLinkText("CRM/SFA").click();
}

@Given("click the Leads")
public void clickTheLeads() {
   driver.findElementByLinkText("Leads").click();
}

@Given("click the Create Lead")
public void clickTheCreateLead() {
   driver.findElementByLinkText("Create Lead").click();
}

@Given("Enter the company name as (.*)")
public void enterTheCompanyNameAsIBM(String CMname) {
    driver.findElementById("createLeadForm_companyName").sendKeys(CMname);
}

@Given("Enter the First name as (.*)")
public void enterTheFirstNameAsPrem(String FName) {
	driver.findElementById("createLeadForm_firstName").sendKeys(FName);
}

@Given("Enter the Last name as (.*)")
public void enterTheLastNameAsKumar(String LName) {
	driver.findElementById("createLeadForm_lastName").sendKeys(LName);
}

@When("click on the Create Lead button")
public void clickOnTheCreateLeadButton() {
    driver.findElementByName("submitButton").click();
}

@Then("verify one Lead has been created")
public void verifyOneLeadHasBeenCreated() {
	WebElement findElementById = driver.findElementById("viewLead_firstName_sp");
	String text = findElementById.getText();
	if(text.contains("prem"))
   System.out.println("Successfully CL has created with Prem");
	else
		System.out.println("Name mismatch");
}



}
*/