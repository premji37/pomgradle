package CreateLeadSteps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import wdMethods.SeMethods;

public class Hooks extends SeMethods  {
	@Before
	public void before(Scenario sc) {
		//cucumber concepts
		/*System.out.println();
		System.out.println(sc.getName());// It will get the particular test case name in cucumber 
		System.out.println(sc.getId());// It will give the exact line number
*/		
		startResult();
		startTestModule(sc.getName(), sc.getId());
		test = startTestCase(sc.getName());
		test.assignCategory("functional");
		test.assignAuthor("prem");
		startApp("firefox", "http://leaftaps.com/opentaps/control/main");
	}
	
	@After
	public void after(Scenario sc) {
		//cucumber concepts
		/*System.out.println(sc.getStatus());// It will say whether the test case is passed or failed during runtime
		System.out.println(sc.isFailed());// Even though if it is failed it will pass the case and its boolean 
		//The get status () has three thing passed or failed or undefined, when the test case has skipped then it will 
		//goes to undefined.
*/		closeAllBrowsers();
        endResult();
		}
	

}
