Feature: Create Test Lead
#Background:
#Given Open The Browser 
#And Max the Browser
#And Set the TimeOut
#And Launch The URL

Scenario Outline: Create Lead

And Enter the UserName as <username>
And Enter the Password as <password>
And click the Login button
And click the CRM Link
And click the Leads
And click the Create Lead
And Enter the company name as <CPName>
And Enter the First name as <FName>
And Enter the Last name as <LName>
When click on the Create Lead button
Then verify the first name <FName>


Examples:
|username|password|CPName|FName|LName|
|DemoSalesManager|crmsfa|IBM|Prem|Kumar|
|DemoSalesManager|crmsfa|HCL|Vino|Kumar|


#Scenario: Only for Login
#And Enter the UserName as DemoSalesManager
#And Enter the Password as crmsfa
#And click the Login button