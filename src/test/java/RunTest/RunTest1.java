package RunTest;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src\\test\\java\\Features\\CreateLead.feature",/*dryRun=true,snippets = SnippetType.CAMELCASE*/
glue= {"CreateLeadSteps","pages"},monochrome=true)

public class RunTest1 {

}