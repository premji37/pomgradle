package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLead extends ProjectMethods {
	public static  String text3;
	public FindLead() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(how = How.NAME, using = "firstName")WebElement elefn1;
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']")WebElement eleclickfindlead;
	@FindBy(how = How.CLASS_NAME, using = "linktext")WebElement elefirstcell;
	@FindBy(how = How.NAME, using = "submitButton")WebElement eleCreateLead;
	@FindBy(how = How.NAME, using = "firstName")WebElement elefn2;
	@FindBy(how = How.XPATH, using = "//input[@name='id']")WebElement eleclickLeadID;
	@FindBy(how = How.XPATH, using = "//div[text()='No records to display']")WebElement eleverifylead;
	public FindLead switchTOFindWindow() throws InterruptedException {	
		Thread.sleep(1000);
		switchToWindow(1);
		return this;
	}
  public FindLead clickfn1(String fn1) {
	  	type(elefn1,fn1);
		return this;
	}
  public FindLead clickFindLead() {		
		click(eleclickfindlead);
		return this;
	}
  
  public FindLead getLeadId() {	
	   text3 = getText(elefirstcell);
	   	return this;
	}
  public Mergelead clickfirstcell2() {	
		 // text3 = getText(elefirstcell);
		  clickWithNoSnap(elefirstcell);
		  
		   	return new Mergelead();
		}
  public FindLead clickfn2(String fn2) {
	  	type(elefn2,fn2);
		return this;
	}

  public FindLead clickLeadID() {
	  type(eleclickLeadID,/*clickfirstcell().getText(elefirstcell)*/text3);
		return this;
	}
  public FindLead VerifyLeadID() {
	  verifyExactText(eleverifylead, "No records to display");
		return this;
	}

}
