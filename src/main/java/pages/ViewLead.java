package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods{

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(how = How.ID, using = "viewLead_firstName_sp")WebElement elefirstname;
	@FindBy(how = How.LINK_TEXT, using = "Find Leads")WebElement eleclickFN;
	@And("verify the first name (.*)")
  public ViewLead verifyfirstname(String data) {		
		verifyExactText(elefirstname, data);
		return this;
	}
	public FindLead clickFN() {		
		click(eleclickFN);
		return new FindLead();
	}

  

}
