package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MyLeads extends ProjectMethods {

	public MyLeads() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(how = How.LINK_TEXT, using = "Create Lead")WebElement eleCL;
	@FindBy(how = How.LINK_TEXT, using = "Merge Leads")WebElement eleML;
	@And("click the Create Lead")
  public CreateLead clickcreatelead() {		
		click(eleCL);
		return new CreateLead();
	}

  public Mergelead Mergeleads() {		
		click(eleML);
		return new Mergelead();
	}




}
