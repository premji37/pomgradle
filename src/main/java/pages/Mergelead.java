package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Mergelead extends ProjectMethods{
	
	public Mergelead() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(how = How.XPATH, using = "//img[@src='/images/fieldlookup.gif']")WebElement eleicon1;
	@FindBy(how = How.ID, using = "createLeadForm_firstName")WebElement eleFirstName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName")WebElement eleLastName;
	@FindBy(how = How.XPATH, using = "//input[@name='partyIdTo']/following::a")WebElement eleicon2;
	@FindBy(how = How.LINK_TEXT, using = "Merge")WebElement elemergebtn;
	@FindBy(how = How.ID, using = "viewLead_firstName_sp")WebElement elefirstname;
	
  public FindLead clickicon1() {		
		click(eleicon1);
		return new FindLead();
	}
  /*public String clickicon1attribute() {		
	  String text = getText(eleicon1);
		
		return text;
	}*/
  
  public Mergelead switchToMergeWindow() throws InterruptedException {	
	  Thread.sleep(1000);
	  switchToWindow(0);
		return this;
	}
  public Mergelead gettextmessage() {	
	   String text4 = getText(eleicon1);
		return this;
	}
  public FindLead clickicon2() {
	  click(eleicon2);
		return new FindLead();
	}
  public Mergelead clickMergeButton() {		
		click(elemergebtn);
		return this;
	}
  public Mergelead clickMergeAlert() throws InterruptedException {	
	  Thread.sleep(1000);
		acceptAlert();
		Thread.sleep(1000);
		return this;
	}
  public ViewLead Verifyfn1() throws InterruptedException {	
	  Thread.sleep(5000);
	  getText(elefirstname);
		return new ViewLead();
  
}
  
}