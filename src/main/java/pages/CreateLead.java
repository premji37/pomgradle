package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	public CreateLead() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(how = How.ID, using = "createLeadForm_companyName")WebElement eleCompanyName;
	@FindBy(how = How.ID, using = "createLeadForm_firstName")WebElement eleFirstName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName")WebElement eleLastName;
	@FindBy(how = How.NAME, using = "submitButton")WebElement eleCreateLead;
	
	@And("Enter the company name as (.*)")	
  public CreateLead enterCompanyName(String companyname) {		
		type(eleCompanyName, companyname);
		return new CreateLead();
	}
	@And("Enter the First name as (.*)")
  public CreateLead enterFirstName(String FirstName) {		
		type(eleFirstName,FirstName);
		return new CreateLead();
	}
	@And("Enter the Last name as (.*)")
  public CreateLead enterLastName(String LastName) {		
	  type(eleLastName,LastName);
		return new CreateLead();
	}
	@And("click on the Create Lead button")
  public ViewLead clickCreateLead() {		
		click(eleCreateLead);
		return new ViewLead();
	}


}
