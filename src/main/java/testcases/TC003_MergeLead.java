package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_MergeLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_MergeLead";
		testDescription = "MergeLead";
		authors = "prem";
		category = "smoke";
		dataSheetName = "TC003";
		testNodes = "MergeLead";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName, String password, String fn1,String fn2) throws InterruptedException {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin().clickCRM().clicklead()
		.Mergeleads().clickicon1().switchTOFindWindow()
		.clickfn1(fn1).clickFindLead().getLeadId().clickfirstcell2().switchToMergeWindow().clickicon2().switchTOFindWindow()
		.clickfn2(fn2).clickFindLead().clickfirstcell2().switchToMergeWindow().clickMergeButton().clickMergeAlert()
		.Verifyfn1().clickFN().clickLeadID().clickFindLead().VerifyLeadID();
		
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	


}
