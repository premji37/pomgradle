package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLead;
import pages.HomePage;
import pages.LoginPage;
import pages.MyHomePage;
import pages.MyLeads;
import pages.ViewLead;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLead";
		authors = "prem";
		category = "smoke";
		dataSheetName = "TC002";
		testNodes = "CreateLead";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName, String password,String CompanyName,String FirstName, String LastName) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin().clickCRM().clicklead()
		.clickcreatelead()
		.enterCompanyName(CompanyName).enterFirstName(FirstName).enterLastName(LastName).clickCreateLead()
		.verifyfirstname(FirstName);
		
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	

	}

